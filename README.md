# README #
This repository contains all the files necessary to recreate gene expression analysis found in the article Upregulation of MLK4 promotes migrat
ory and invasive potential of breast cancer cells. The repository contains following directories:
1. Scripts - where both scripts used for differential gene expression analysis and data extraction from cBio portal are located. cbio_github.R contains only raw code, an explanation of the analysis is provided in a corresponding markdown. 
2. Supplement - where information is provided to which breast cancer subtype a given sample was assign. Additionally a column 'Passed_QC' informs whether a given sample passed our validation criteria (1-Passed, 0-failed). For each study a separate file is provided.
3. Example. This directory contains the script (script.R) and input (input.txt) files nescessery to recreate anakysis for E-GEOD-21653 dataset.  All the plots created during the analysis are alos provided for comparison. 
4. The repository DOES NOT contain raw .CEL files These can be downloaded directly from Array Express website or using R command getAE from 'arrayexpress' package.  

If you have any comments/suggestions you can use the following email for contact:
m.lazniewski@cent.uw.edu.pl
